## php7-phalcon

This docker container provides php-cli and php-cgi with all modules you will need including phalcon 3.0.x.

It is designed to create own container from it for specific proposes. For Example iras/php-fpm-phalcon is using it:

```
FROM iras/php7-phalcon:latest

RUN apt-get update \
 && apt-get install -yq \
      php7.0-fpm
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# maybe some more configuration stuff

EXPOSE 9000

CMD ["/usr/sbin/php-fpm"]
```
