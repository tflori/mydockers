#!/bin/bash

set -e

# add the phalcon repository
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | bash

# install phalcon
apt-get install -yq --no-install-recommends \
  php7.0-phalcon

# clean up
apt-get autoremove -yq
apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/bin/phalcon
