## php7-phalcon-test

This container is just php7-phalcon with additional composer and phpunits installed to run automated tests.

### PhpStorm

To use this container in phpstorm you have to create two files. First file is an executable file that runs this
container and the second is just to tell phpstorm where the phpunit loader is located.

The executable file (for example `/usr/local/bin/php7-phalcon-test`):
```bash
#!/usr/bin/env bash

# phpstorm runs this file in this form:
# /path/to/file /tmp/ide-phpunit.php --configuration /path/to/phpunit.xml

# we have to give /tmp/ide-phpunit.php and the working directory to the container
docker run -t --rm -v "$1":"$1" -v "${PWD}":"${PWD}" --net=host \
    -e IDE_PHPUNIT_PHPUNIT_PHAR \
    -e IDE_PHPUNIT_CUSTOM_LOADER \
    -w "${PWD}" iras/php7-phalcon-test php "$@"
```

The second file just have to exist: `touch /usr/local/bin/phpunit`.

Then you have to set the php interpreter to the executable (make sure it is executable
`chmod +x /usr/local/bin/php7-phalcon-test`) and the phpunit custom autoloader to `/usr/local/bin/phpunit`.
