## docker-compose

The docker-compose container is for running docker-compose on a remote docker. I'm using it for testing. You may want
to link a docker container (e. g. docker:dind) under the name docker. The Environment variable DOCKER_HOST is set to
`tcp://docker:2375`.
